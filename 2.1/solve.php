<?php

	$data=file('data.txt',FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	$hor=$depth=0;
	for ($l=0;$l<sizeof($data);++$l)
	{
		list($dir,$amt)=preg_split("/\s+/",$data[$l],2);
		switch ($dir)
		{
			case 'forward':
				$hor+=intval($amt);
				break;
			case 'down':
				$depth+=intval($amt);
				break;
			case 'up':
				$depth-=intval($amt);
				break;
			default:
				echo "Unknown direction: ".$dir."\n";
		}
	}
	echo "Horizontal position: ".intval($hor)."\n";
	echo "Depth: ".intval($depth)."\n";
	echo "Result: ".round($hor*$depth)."\n";


?>