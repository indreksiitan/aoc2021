<?php

	$data=file('data.txt',FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	$hor=$aim=$depth=0;
	for ($l=0;$l<sizeof($data);++$l)
	{
		list($dir,$amt)=preg_split("/\s+/",$data[$l],2);
		switch ($dir)
		{
			case 'down':
				$aim+=intval($amt);
				break;
			case 'up':
				$aim-=intval($amt);
				break;
			case 'forward':
				$hor+=intval($amt);
				$depth+=round($aim*$amt);
				break;
			default:
				echo "Unknown direction: ".$dir."\n";
		}
	}
	echo "Horizontal position: ".intval($hor)."\n";
	echo "Depth: ".intval($depth)."\n";
	echo "Aim: ".intval($aim)."\n";
	echo "Result: ".round($hor*$depth)."\n";


?>